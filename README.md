# Bulk Sync Child/Grandchild Records

## Overview and Reasoning

Because Salesforce is trying to move its declarative focus towards Flows, we try to build it there instead of in the Process Builder. There's two limitations to overcome.

1. There's currently a limitation (that is removed in an upcoming release) that prevents record-triggered flows from accessing the old values after an update. Therefore, we cannot conviniently check if the Status field on Master has been updated from that context. Because of this, we use the Process Builder to check if the value is changed, and then launch the Sync flow from there.

1. There is no existing declarative equivilent for the SOQL statement "*...WHERE Id IN :idList*". Therefore, to update the Line Item (grandchild) records without hitting governer limits, we break the flow up into two parts: (1) when a Master Status is updated, which updates its children Materials. (2) when Material Status is updated, which updates its children Line Items.

### Process Builder

#### On Master

![Process Builder on Master](media/pb-master.png)

#### On Material

![Process Builder on Material](media/pb-material.png)

### Flow Builder

#### Material Sync

![Flow for Material Sync](media/flow-material.png)

#### Line Item Sync

![Flow for Line Item Sync](media/flow-lineitem.png)


## Scaling

To confirm governor limits will not be an issue, we run tests with a varying number of Materials and Line Items to test some extremes and normal cases. This shows that our limit usage remains constant in varying situations.

| Materials | Line Items Per Material | SOQL Queries | DML Statements |
|-----------|-------------------------|--------------|----------------|
| 500       | 1                       | 6/100        | 3/150          |
| 200       | 2                       | 6/100        | 3/150          |
| 10        | 100                     | 6/100        | 3/150          |
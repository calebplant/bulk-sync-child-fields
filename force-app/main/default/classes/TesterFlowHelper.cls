public with sharing class TesterFlowHelper {
    @InvocableMethod(label='tester flow helper' description='Debugging util function')
    public static void execute (List<Requests> requestList) {
        System.debug('START tester flow helper');
        System.debug('Cons: ');
        if(requestList[0].cons != null) {
            for(Contact eachCon : requestList[0].cons) {
                System.debug(eachCon.Name);
            }
        }
        System.debug('END tester flow helper');
    }

    public class Requests {
        @InvocableVariable(label='cons' description='desc' required=false)
        public List<Contact> cons; 
    }
}

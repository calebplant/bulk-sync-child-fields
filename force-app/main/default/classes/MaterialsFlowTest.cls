@isTest
public class MaterialsFlowTest {

    private static final String ROOT_MASTER_NAME = 'MyMaster';
    private static final Integer NUM_OF_MATERIALS = 100;
    private static final Integer NUM_OF_LINE_ITEMS_PER_MAT = 100;

    @TestSetup
    static void makeData(){
        // Master
        Master__c master = new Master__c(Name=ROOT_MASTER_NAME, Status__c='Oldest');
        insert master;
        // Materials
        List<Material__c> materials = new List<Material__c>();
        for(Integer i=0; i < NUM_OF_MATERIALS; i++) {
            materials.add(new Material__c(Name='material'+i, Status__c='Old', Master__c=master.Id));
        }
        insert materials;
        // Line Items
        List<Line_Item__c> lineItems = new List<Line_Item__c>();
        for(Material__c eachMaterial : materials) {
            for(Integer i=0; i < NUM_OF_LINE_ITEMS_PER_MAT; i++) {
                lineItems.add(new Line_Item__c(Name='lineItem'+i, Status__c='Older', Material__c=eachMaterial.Id));
            }
        }
        insert lineItems;
    }

    @isTest
    static void MyTester()
    {
        Master__c master = [SELECT Id, Status__c FROM Master__c WHERE Name=:ROOT_MASTER_NAME LIMIT 1];
        Map<Id, Material__c> materials = new Map<Id, Material__c>([SELECT Id FROM Material__c
                                                                    WHERE Master__r.Id = :master.Id]);
        Map<Id, Line_Item__c> lineItems = new Map<Id, Line_Item__c>([SELECT Id FROM Line_Item__c
                                                                    WHERE Material__r.Id IN :materials.keySet()]);
        System.debug('Master: ' + master);
        System.debug('Material: ' + materials.size());
        System.debug('Line Item: ' + lineITems.size());
        Test.startTest();
        master.Status__c = 'New';
        update master;
        Test.stopTest();
        

        master = [SELECT Id, Status__c, Synced_Value_Changed__c FROM Master__c WHERE Name=:ROOT_MASTER_NAME LIMIT 1];
        materials = new Map<Id, Material__c>([SELECT Id, Status__c FROM Material__c
                                                WHERE Master__r.Id = :master.Id]);
        lineItems = new Map<Id, Line_Item__c>([SELECT Id, Status__c FROM Line_Item__c
                                        WHERE Material__r.Id IN :materials.keySet()]);
        System.debug('Master: ' + master);
        System.debug('Material: ');
        for(Material__c eachMaterial : materials.values()) {
            System.debug(eachMaterial);
        }
        System.debug('Line Item: ');
        for(Line_Item__c eachLineItem : lineItems.values()) {
            System.debug(eachLineItem);
        }
    }

    @isTest
    static void testerFlow()
    {
        Account acc = new Account(Name='acc1');
        insert acc;
        insert new Contact(LastName='con1', AccountId=acc.Id);
        insert new Contact(LastName='con2', AccountId=acc.Id);
        Test.startTest();
        acc.Name = 'newAcc1';
        update acc;
        Test.stopTest();
    }
}

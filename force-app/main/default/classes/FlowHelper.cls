public with sharing class FlowHelper {

    @InvocableMethod(label='Debug Util' description='Debugging util function')
    public static void execute (List<Requests> requestList) {
        System.debug('START Debug Util');
        // System.debug('Materials:');
        // for(Material__c x : requestList[0].materials) {
        //     System.debug(x);
        // }
        if(requestList.size() > 0) {
            System.debug(requestList);
            if(requestList[0].lineItems != null) {
                System.debug('Line Items:');
                for(Line_Item__c x : requestList[0].lineItems) {
                    System.debug(x);
                }
            }

        }

        // List<SObject> oldCollection = requestList[0].oldCollection;
        // List<SObject> newCollection = requestList[0].newCollection;
        // System.debug(requestList[0].oldCollection);
        // System.debug(requestList[0].newCollection);
        System.debug('END Debug Util');
    }

    public class Requests {
        // @InvocableVariable(label='Old master' description='desc' required=true)
        // public Master__c oldCollection;
        // @InvocableVariable(label='New master' description='desc' required=true)
        // public Master__c newCollection;
        // @InvocableVariable(label='children' description='desc' required=true)
        // public List<Material__c> materials;
        @InvocableVariable(label='grandchildren' description='desc' required=false)
        public List<Line_Item__c> lineItems; 
    }
}
